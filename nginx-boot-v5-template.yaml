apiVersion: template.openshift.io/v1
kind: Template
labels:
  template: nginx-java
metadata:
  annotations:
    description: An example Nginx HTTP server and a reverse proxy (nginx) application
      backed by Java that serves static content. For more information about using
      this template, including OpenShift considerations, see https://gitlab.com/changdukkim/nginx-boot/README.md
    iconClass: icon-nginx
    openshift.io/display-name: OpenJDK and Nginx HTTP server and a reverse proxy
    openshift.io/documentation-url: https://gitlab.com/changdukkim/nginx-boot
    openshift.io/long-description: This template defines resources needed to develop
      a static application served by Nginx HTTP server and a reverse proxy (nginx)
      and Java(OpenJDK) including a build configuration and application deployment
      configuration.
    tags: quickstart,nginx,springboot
  creationTimestamp: 2019-07-03T06:54:49Z
  name: nginx-java
  namespace: openshift
  resourceVersion: "106131"
  selfLink: /apis/template.openshift.io/v1/namespaces/openshift/templates/nginx-java
  uid: 737367f9-9d5f-11e9-8004-02f2d6f14fd0
objects:
- apiVersion: v1
  kind: ImageStream
  metadata:
    labels:
      application: ${APPLICATION_NAME}
    name: ${APPLICATION_NAME}
- apiVersion: v1
  kind: ImageStream
  metadata:
    labels:
      application: ${BACKEND_APPLICATION_NAME}
    name: ${BACKEND_APPLICATION_NAME}
- apiVersion: v1
  kind: Service
  metadata:
    annotations:
      description: The web server's http port.
    labels:
      application: ${APPLICATION_NAME}
    name: ${APPLICATION_NAME}
  spec:
    ports:
    - port: 8080
      targetPort: 8080
    selector:
      deploymentConfig: ${APPLICATION_NAME}
- apiVersion: v1
  id: ${APPLICATION_NAME}-http
  kind: Route
  metadata:
    annotations:
      description: Route for application's http service.
    labels:
      application: ${APPLICATION_NAME}
    name: ${APPLICATION_NAME}
  spec:
    to:
      name: ${APPLICATION_NAME}
- apiVersion: v1
  kind: BuildConfig
  metadata:
    labels:
      application: ${BACKEND_APPLICATION_NAME}
    name: ${BACKEND_APPLICATION_NAME}
  spec:
    output:
      to:
        kind: ImageStreamTag
        name: ${BACKEND_APPLICATION_NAME}:latest
    source:
      git:
        ref: ${SOURCE_REF}
        uri: ${SOURCE_URL}
      type: Git
    strategy:
      sourceStrategy:
        forcePull: true
        from:
          kind: ImageStreamTag
          name: redhat-openjdk18-openshift:1.4
          namespace: openshift
      type: Source
    triggers:
    - github:
        secret: ${GITHUB_WEBHOOK_SECRET}
      type: GitHub
    - generic:
        secret: ${GENERIC_WEBHOOK_SECRET}
      type: Generic
    - imageChange: {}
      type: ImageChange
    - type: ConfigChange
- apiVersion: v1
  kind: BuildConfig
  metadata:
    labels:
      application: ${APPLICATION_NAME}
    name: ${APPLICATION_NAME}
  spec:
    output:
      to:
        kind: ImageStreamTag
        name: ${APPLICATION_NAME}:latest
    source:
      git:
        ref: ${SOURCE_REF}
        uri: ${SOURCE_URL}
      type: Git
    strategy:
      sourceStrategy:
        forcePull: true
        from:
          kind: ImageStreamTag
          name: nginx:latest
          namespace: openshift
      type: Source
    triggers:
    - github:
        secret: kJZLvfQr3hZg
      type: GitHub
    - generic:
        secret: kJZLvfQr3hZg
      type: Generic
    - imageChange: {}
      type: ImageChange
    - type: ConfigChange
- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    labels:
      application: ${APPLICATION_NAME}
    name: ${APPLICATION_NAME}
  spec:
    replicas: 2
    selector:
      deploymentConfig: ${APPLICATION_NAME}
    strategy:
      type: Rolling
    template:
      metadata:
        labels:
          application: ${APPLICATION_NAME}
          deploymentConfig: ${APPLICATION_NAME}
        name: ${APPLICATION_NAME}
      spec:
        containers:
        - env: null
          image: ${APPLICATION_NAME}
          imagePullPolicy: Always
          livenessProbe:
            httpGet:
              path: /
              port: 8080
            initialDelaySeconds: 30
            timeoutSeconds: 3
          name: ${APPLICATION_NAME}
          ports:
          - containerPort: 8080
            name: http
            protocol: TCP
          readinessProbe:
            httpGet:
              path: /
              port: 8080
            initialDelaySeconds: 3
            timeoutSeconds: 3
          resources:
            cpu: 50m
            memory: 256Mi
          limits:
            cpu: ${CORE_LIMITS}
            memory: ${MEMORY_LIMITS}
        - image: ${BACKEND_APPLICATION_NAME}
          imagePullPolicy: Always
          name: ${BACKEND_APPLICATION_NAME}
          ports:
          - containerPort: 18080
            name: http
            protocol: TCP
          resources:
            cpu: 50m
            memory: 256Mi
          limits:
            cpu: ${CORE_LIMITS}
            memory: ${MEMORY_LIMITS} 
    triggers:
    - imageChangeParams:
        automatic: true
        containerNames:
        - ${BACKEND_APPLICATION_NAME}
        from:
          kind: ImageStream
          name: ${BACKEND_APPLICATION_NAME}
      type: ImageChange
    - imageChangeParams:
        automatic: true
        containerNames:
        - ${APPLICATION_NAME}
        from:
          kind: ImageStream
          name: ${APPLICATION_NAME}
      type: ImageChange
    - type: ConfigChange
parameters:
- description: The name for the Nginx application.
  name: APPLICATION_NAME
  required: true
  value: c-bmt-frontend
- description: The name for the Java application.
  name: BACKEND_APPLICATION_NAME
  required: true
  value: c-bmt-backend
- description: Git source URI for application
  name: SOURCE_URL
  required: true
  value: https://gitlab.com/changdukkim/nginx-boot.git
- description: Git branch/tag reference
  name: SOURCE_REF
  value: master
- description: Maximum amount of core the container can use.
  displayName: Core Limit
  name: CORE_LIMIT
  required: true
  value: "1"
- description: Maximum amount of memory the container can use.
  displayName: Memory Limit
  name: MEMORY_LIMIT
  required: true
  value: "2Gi"
- description: GitHub trigger secret
  displayName: Github Webhook Secret
  from: '[a-zA-Z0-9]{8}'
  generate: expression
  name: GITHUB_WEBHOOK_SECRET
  required: true
- description: Generic build trigger secret
  displayName: Generic Webhook Secret
  from: '[a-zA-Z0-9]{8}'
  generate: expression
  name: GENERIC_WEBHOOK_SECRET
  required: true

